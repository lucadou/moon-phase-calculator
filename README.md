# Moon Phase Calculator

This is a small C++ program that determines the current phase of the moon and
outputs it to the terminal, along with some nice ASCII art.

![A screenshot of the application running in the terminal, surrounded by output from other commands](img/screenshot.png)

### Getting Started

This application has no configuration to mess with. Just compile and run.

### Dependency Setup

This should work with any Debian derivative due to how generic the packages are,
but I know this works with Ubuntu 20.04 since that is my dev environment.

```bash
sudo apt install gcc build-essential gcc-doc
```

### Building

```bash
cd mooncalc
make mooncalc
```

### Running

```bash
./mooncalc
```

## Notes/FAQ

> Why did you make this?

I wanted a fun greeting whenever I logged onto my university's research
cluster. Something to help break up the monotony of the cycle of *push...pull...
queue...debug...push...pull...queue...*

> What is the icon for this project?

It's the Apple First Quarter Moon emoji.

> Where did you find the algorithm and ASCII art?

The [mooncalc file](mooncalc/mooncalc.cpp) has credits in it.

## Versioning

This project uses Semantic Versioning, aka [SemVer](https://semver.org/spec/v2.0.0.html).

## Authors

* Luna Lucadou - Wrote the application in her free time

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details.

## Bugs, feature ideas, etc?

Feel free to fill out an issue or submit a merge request!

