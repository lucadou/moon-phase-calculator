# Changelog
All notable changes to this project will be documented in this file.

## [1.0.2] - 2022-03-15
### Changed
- Changelog
- Long ints to long long ints (force 64-bit; see year 2038 problem)
- Updated screenshot

## [1.0.1] - 2021-01-31
### Added
- Changelog
- License

### Changed
- Added stuff to readme

## [1.0.0] - 2021-01-31
### Added
- Readme
- Makefile
- Working application

