/**
 * Moon Calculator - Determines the current phase of the moon
 * and outputs it with some nice ASCII art.
 *
 * Copyright (C) Luna Lucadou 2021
 * https://gitlab.com/lucadou/moon-phase-calculator
 * https://luna.lucadou.sh/
 *
 * Credits:
 * Algorithm - Minkukel Plus (https://minkukel.com/en/various/calculating-moon-phase/)
 * ASCII art - "jgs" (http://www.ascii-art.de/ascii/mno/moon.txt)
 */

#include <chrono>
#include <cmath>
#include <iostream>
#include <string>

int main() {
  // Constants
  // Output-related constants
  const int MOON_SIZE = 6;
  std::string MOON_ASCII[10][MOON_SIZE] = {
    {"        _..._", "      .:::::::.", "     :::::::::::", "     :::::::::::", "     `:::::::::'", "       `':::''"},
    {"        _..._", "      .::::. `.", "     :::::::.  :", "     ::::::::  :", "     `::::::' .'", "       `'::'-'"},
    {"        _..._", "      .::::  `.", "     ::::::    :", "     ::::::    :", "     `:::::   .'", "       `'::.-'"},
    {"        _..._", "      .::'   `.", "     :::       :", "     :::       :", "     `::.     .'", "       `':..-'"},
    {"        _..._", "      .'     `.", "     :         :", "     :         :", "     `.       .'", "       `-...-'"},
    {"        _..._", "      .'   `::.", "     :       :::", "     :       :::", "     `.     .::'", "       `-..:''"},
    {"        _..._", "      .'  ::::.", "     :    ::::::", "     :    ::::::", "     `.   :::::'", "       `-.::''"},
    {"        _..._", "      .' .::::.", "     :  ::::::::", "     :  ::::::::", "     `. '::::::'", "       `-.::''"},
    {"        _..._", "      .:::::::.", "     :::::::::::", "     :::::::::::", "     `:::::::::'", "       `':::''"},
    {"", "", "", "", "", ""}
  };
  const std::string MOON_PHASES[10] = {
    "New Moon",
    "Waxing Crescent",
    "First Quarter",
    "Waxing Gibbous",
    "Full Moon",
    "Waning Gibbous",
    "Last Quarter",
    "Waning Crescent",
    "New Moon",
    "Unknown Moon"
  };
  const std::string TONIGHT_MSG = "Tonight, the moon will be a";
  // Lunar constants
  // Minkukel links to http://individual.utoronto.ca/kalendis/lunar/#FALC
  // for an explanation where LUNAR_DAYS comes from
  const double LUNAR_DAYS = 29.53058770576;
  const double LUNAR_SECS = LUNAR_DAYS * (24 * 60 * 60);
  const char* START_DATE = "2000-01-06 18:14:00";
  const char* SD_TIME_FMT = "%Y-%m-%d %T";

  // Get current date+time (as UNIX timestamp)
  auto c_now = std::chrono::system_clock::now();
  long long int c_epoch = std::chrono::duration_cast<std::chrono::seconds>(c_now.time_since_epoch()).count();

  // Convert the first new moon in 2000 to UNIX timestamp
  // Credit: https://stackoverflow.com/a/21021900
  std::tm nm_2k_tmp = {};
  strptime(START_DATE, SD_TIME_FMT, &nm_2k_tmp);
  auto nm_2k = std::chrono::system_clock::from_time_t(std::mktime(&nm_2k_tmp));
  long long int fm_epoch = std::chrono::duration_cast<std::chrono::seconds>(nm_2k.time_since_epoch()).count();

  // Get time between now and first new moon of 2000
  long long int delta = c_epoch - fm_epoch;
  double c_cycle_secs = fmod(delta, LUNAR_SECS);

  if (c_cycle_secs < 0) {
    c_cycle_secs += LUNAR_SECS;
  }

  // Calculate seconds passed in current cycle
  double c_frac = c_cycle_secs / LUNAR_SECS;
  double c_days = c_frac * LUNAR_DAYS;

  // Assign values for output
  std::string phase;
  std::string* art;
  if (c_days >= 0 && c_days <= 1) {
    phase = MOON_PHASES[0];
    art = MOON_ASCII[0];
  } else if (c_days > 1 && c_days <= 6.38264692644) {
    phase = MOON_PHASES[1];
    art = MOON_ASCII[1];
  } else if (c_days > 6.38264692644 && c_days <= 8.38264692644) {
    phase = MOON_PHASES[2];
    art = MOON_ASCII[2];
  } else if (c_days > 8.38264692644 && c_days <= 13.76529385288) {
    phase = MOON_PHASES[3];
    art = MOON_ASCII[3];
  } else if (c_days > 13.76529385288 && c_days <= 15.76529385288) {
    phase = MOON_PHASES[4];
    art = MOON_ASCII[4];
  } else if (c_days > 15.76529385288 && c_days <= 21.14794077932) {
    phase = MOON_PHASES[5];
    art = MOON_ASCII[5];
  } else if (c_days > 21.14794077932 && c_days <= 23.14794077932) {
    phase = MOON_PHASES[6];
    art = MOON_ASCII[6];
  } else if (c_days > 23.14794077932 && c_days <= 28.53058770576) {
    phase = MOON_PHASES[7];
    art = MOON_ASCII[7];
  } else if (c_days > 28.53058770576 && c_days <= 29.53058770576) {
    phase = MOON_PHASES[8];
    art = MOON_ASCII[8];
  } else {
    phase = MOON_PHASES[9] + ": c_days=" + std::to_string(c_days);
    art = MOON_ASCII[9];
  }

  // Output to terminal
  for (int i = 0; i < MOON_SIZE; i++) {
    std::cout << art[i];
    if (i == 2) {
      std::cout << "    " << TONIGHT_MSG;
    } else if (i == 3) {
      std::cout << "    " << phase;
    }
    std::cout << std::endl;
  }

  return 0;
}
